alias gcfge="git config user.email"
alias gcfgn="git config user.name"

alias ga="git add"
alias gaa="git add -A"
alias gc="git commit -m"
alias gpsh="git push"
alias gpll="git pull"

alias grao="git remote add origin"
alias grsu="git remote set-url"

git_auto() {
  if [ "$#" -eq 0 ]; then
    echo "commit message not provided"
    return
  fi

  gaa & gc $1 && gpsh
}
