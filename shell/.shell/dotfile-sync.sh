sync-dotfiles() {
  calldir=$(pwd) 
  cd ~/dotfiles/ &&

  git add .
  git commit -m "synced on: $(date)"
  git push -u origin --all

  cd $calldir
}
